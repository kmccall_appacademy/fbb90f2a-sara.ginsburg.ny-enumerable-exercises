require 'byebug'

# EASY

# Define a method that returns the sum of all the elements in its argument (an
# array of numbers).
def array_sum(arr)
  return 0 if arr == []
  return arr.reduce(:+)
end

# Define a method that returns a boolean indicating whether substring is a
# substring of each string in the long_strings array.
# Hint: you may want a sub_tring? helper method
def in_all_strings?(long_strings, substring)
  long_strings.each do |word|
    return false if !(sub_string?(word, substring))
  end
  return true
end

def sub_string?(long_str, str)
  long_str.include?(str)
end

# Define a method that accepts a string of lower case words (no punctuation) and
# returns an array of letters that occur more than once, sorted alphabetically.
def non_unique_letters(string)
  string_arr = string.split("")
  result = string_arr.select {|word| word != " " && string_arr.count(word) > 1}
  return result.uniq
end

# Define a method that returns an array of the longest two words (in order) in
# the method's argument. Ignore punctuation!
def longest (string)
  string.split(" ").reduce do |longest, word|
    if word.length > longest.length
      word
    else
      longest
    end
  end
end

def longest_two_words(string)
  result = [longest(string)]
  new_arr = string.split(" ").reject {|word| word == longest(string)}
  result.push(longest(new_arr.join(" ")))
  return result
end

# MEDIUM

# Define a method that takes a string of lower-case letters and returns an array
# of all the letters that do not occur in the method's argument.
def missing_letters(string)
  ("a".."z").reduce([]) do |missing, char|
     if !string.include?(char)
       missing << char
     else
       missing
     end
  end
end

# Define a method that accepts two years and returns an array of the years
# within that range (inclusive) that have no repeated digits. Hint: helper
# method?
def no_repeat_years(first_yr, last_yr)
  (first_yr..last_yr).reduce([]) do |years, year|
    if not_repeat_year?(year)
      years << year
    else
      years
    end
  end
end

def not_repeat_year?(year)
  year.to_s.chars.uniq == year.to_s.chars
end

# HARD

# Define a method that, given an array of songs at the top of the charts,
# returns the songs that only stayed on the chart for a week at a time. Each
# element corresponds to a song at the top of the charts for one particular
# week. Songs CAN reappear on the chart, but if they don't appear in consecutive
# weeks, they're "one-week wonders." Suggested strategy: find the songs that
# appear multiple times in a row and remove them. You may wish to write a helper
# method no_repeats?
def one_week_wonders(songs)
  result = populate_week_wonders(songs)
  return result.uniq
end

def populate_week_wonders(songs)
  songs.reduce([]) do |wonder_songs, song|
    if no_repeats?(song, songs)
      wonder_songs << song
    else
      wonder_songs
    end
  end
 end


def no_repeats?(song_name, songs)
  song_idx = songs.each_with_index.map { |song, i| song == song_name ? i : nil }.compact
  song_idx.reduce do |compare, idx|
    if compare + 1 == idx
      return false
    else
      idx
    end
  end
  return true
end

# Define a method that, given a string of words, returns the word that has the
# letter "c" closest to the end of it. If there's a tie, return the earlier
# word. Ignore punctuation. If there's no "c", return an empty string. You may
# wish to write the helper methods c_distance and remove_punctuation.

def for_cs_sake(string)
  words_arr = remove_punctuation(string)
  words_arr.reduce do |the_word, word|
    if c_distance(word) < c_distance(the_word)
       word
    else
      the_word
    end
  end
end

def c_distance(word)
  return word.length if word.index("c") == nil
  return word.length - word.index("c")
end

def remove_punctuation(string)
  string.scan(/[a-z']+/i)
end


# Define a method that, given an array of numbers, returns a nested array of
# two-element arrays that each contain the start and end indices of whenever a
# number appears multiple times in a row. repeated_number_ranges([1, 1, 2]) =>
# [[0, 1]] repeated_number_ranges([1, 2, 3, 3, 4, 4, 4]) => [[2, 3], [4, 6]]

def repeated_number_ranges(arr)

  result = arr.reduce([]) do |pairs, n|
    if repeats(n, arr) != []
      pairs + repeats(n, arr)
    else
      pairs
    end
  end
  return result.uniq.sort
end

def repeats(num, numbers)
  number_idx = numbers.each_with_index.map {|number, i| number == num ? i : nil}.compact
  number_idx_grouped = number_idx.slice_when {|i, j| i + 1 != j}
  groups_array = number_idx_grouped.to_a
  groups_array.map do |arr|
    if arr.length < 2
      nil
    elsif arr.length > 2
      [arr[0],arr[-1]]
    else
      arr
    end
  end.compact
end
